import 'babel-polyfill'

import Generator from 'yeoman-generator'
import yosay from 'yosay'
import { toLower } from 'lodash'

class LetiaGenerator extends Generator {
  welcome() {
    this.log(yosay())
  }

  async prompting() {
    const answers = await this.prompt([{
      type: 'list',
      name: 'language',
      message: 'What\'s your language?',
      choices: ['rust'],
      filter: toLower
    }])
    this.language = answers.language
  }

  forkOnLanguage() {
    switch(this.language) {
      case 'rust':
        this.composeWith('letia:rust')
        break
      default:
        this.log('Unsupported language... yet!')
    }
  }
}

module.exports = LetiaGenerator
