import Generator from 'yeoman-generator'
import chalk from 'chalk'
import { toLower } from 'lodash'
import { execSync } from 'child_process'

const error = chalk.bold.red
const warning = chalk.keyword('orange')
const info = chalk.dim.keyword('cyan')

class RustGenerator extends Generator {
  async prompting() {
    const answers = await this.prompt([{
      type: 'list',
      name: 'type',
      message: 'What do you want to create?',
      choices: ['workspace', 'crate'],
      filter: toLower
    }])
    this.type = answers.type
  }

  async promptingWorkspace() {
    if (this.type !== 'workspace') return

    const answers = await this.prompt([{
      type: 'input',
      name: 'name',
      message: 'What\'s the workspace name?',
      validate: (name) => !!name || 'Workspace name should not be blank'
    }, {
      type: 'confirm',
      name: 'crates',
      message: 'Do you want to create crates inside your workspace?'
    }])
    this.workspace = answers.name
    this.workspaceCrates = answers.crates
  }

  async promptingCrate() {
    if (this.type === 'workspace' && !this.workspaceCrates) return

    this.log(info('Great! Tell me more about your crate...'))
    const crates = []
    let prompting = false
    do {
      const crate = await this.prompt([{
        type: 'list',
        name: 'type',
        choices: ['lib', 'bin']
      }, {
        type: 'input',
        name: 'name',
        validate: (name) => this._validateCrateName(name, crates)
      }, {
        type: 'confirm',
        name: 'prompting',
        message: 'Add more crates?',
        when: !!this.workspaceCrates
      }])

      crates.push(crate)
      prompting = !!crate.prompting
    } while (prompting)

    this.crates = crates
  }

  async writing() {

    if (this.type === 'workspace') {
      this.destinationRoot(this.workspace)
      this.fs.copyTpl(
        this.templatePath('workspace.Cargo.toml'),
        this.destinationPath('Cargo.toml'),
        { crates: this.crates }
      )
      this.fs.copyTpl(
        this.templatePath('gitignore'),
        this.destinationPath('.gitignore')
      )
      execSync('git init')
    }

    const vcs = this.workspace ? '' : '--vcs git'

    this.crates.forEach(crate => {
      execSync(`cargo new ${crate.name} --${crate.type} ${vcs}`)
    })
  }

  _validateCrateName(name, crates) {
    if (!name) return 'Name should not be blank'
    if (crates.some(crate => crate.name === name)) return 'Name already used'
    return true
  }
}

module.exports = RustGenerator
